

/**
 * @file main.cpp
 * @author Nassim Agrebi (technical@nassimengineering.de)
 * @brief
 * @version 0.1
 * @date 2020-11-27
 *
 * @copyright Copyright (c) 2020
 *
 */
//Adjusted esp32-hal-bt.c   https://github.com/espressif/arduino-esp32/issues/2718#issuecomment-552213656
// pio run -t erase

#include <Arduino.h>
#include <ArduinoJson.h>
#include <WiFi.h>
#include <weather.h>
#include <bluetooth.h>
#include <HTTPClient.h>
#include <Wire.h>        // Only needed for Arduino 1.6.5 and earlier
// #include "SSD1306Wire.h" // legacy include: `#include "SSD1306.h"`
#include <time.h>

#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

#include <U8g2lib.h>
#include <FastLED.h>

#include "esp32-hal-bt.h"
#include "esp_bt.h"
#include "esp_wifi.h"


//////////////////////////////////////////////////////////////
//---------------------DEFINES------------------------------//
//////////////////////////////////////////////////////////////
/*LEDS*/
#define LED_PIN     5
#define NUM_LEDS    25
#define BRIGHTNESS  250
#define LED_TYPE    WS2811
#define COLOR_ORDER GRB
CRGB leds[NUM_LEDS];
/*BME280*/
#define SEALEVELPRESSURE_HPA (1013.25)
#define BME280_ADDRESS 0x76
/*Times*/
#define SECOND 1000UL
#define MINUTE (SECOND * 60UL)
#define HOUR (MINUTE * 60UL)
int timezone = 1 * 3600; //GER UTC +1
int dst = 0;
/*PUMP*/
#define PUMP_PIN  27
#define PUMP_MAX_ON 3000 // in ms
/*Bluetooth*/
// Check if Bluetooth configs are enabled
#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

// Bluetooth Serial object
BluetoothSerial SerialBT;
// Handle received and sent messages
String message = "";
char incomingChar;
String temperatureString = "";

/*Openweathermap*/
String CityID = "2911298"; //Hamburg
String APIKEY = "9fc2f6d9361da88c6587b261b927a8de";
// const char *ssid = "FRITZ!Box 7490";
// const char *password = "00706815983242529202";

char ssid[40] = "";
char password[40] = "";

/*handle weather data here*/
OpenWeatherMapCurrentData_t current_data_t;
Adafruit_BME280 bme;
U8G2_SSD1306_128X64_NONAME_1_HW_I2C u8g2(U8G2_R0, /* clock=*/SCL, /* data=*/SDA, /* reset=*/U8X8_PIN_NONE);


/*update Intervall Weather data*/
unsigned long lastCallTime = 0;                    // last time you called the updateWeather function, in milliseconds
const unsigned long postingInterval = 5*MINUTE; // delay between updates, in milliseconds
/*update Intervall pages*/
unsigned long lastCallTime_page = 0;                    // last time you called the updateWeather function, in milliseconds
const unsigned long postingInterval_page = 10*SECOND; // delay between updates
int page_cycles = 0;// to show all for same time
String result;
float Temperature;
int bt_error;
void setup()
{
  /*initalize LED*/
  initLED();
  /*initalize OLED*/
  u8g2.begin();
  /*initalize Serial port*/
  Serial.begin(9600);
  /*set welcome msg*/
  u8g2.setFont(u8g2_font_luRS08_te);
  u8g2.firstPage();
  do
  {
    u8g2.drawStr(0, 20, "Willkommen");
    u8g2.drawStr(0, 30, "checke Sensor...");
  } while (u8g2.nextPage());
  delay(500);
  /*Init BME280*/
  bool status = bme.begin(BME280_ADDRESS);
  if (!status)
  {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    while (1);
  }

/*Start Bluetooth*/
  SerialBT.begin("ESP32");
  u8g2.firstPage();
  do
  {
    u8g2.drawStr(0, 10, "Willkommen");
    u8g2.drawStr(0, 20, "checke Sensor...ok");
    u8g2.drawStr(0, 30, "Bluetooth....aktiviert");
    u8g2.drawStr(0, 40, "ssid & pw eingeben...");
  } while (u8g2.nextPage());

  bl_get_WiFi(ssid,password,&SerialBT);
  SerialBT.end();
  /*connect to WiFi*/
  connectToWifi();
  u8g2.firstPage();
  do
  {
    u8g2.drawStr(0, 10, "Willkommen");
    u8g2.drawStr(0, 20, "checke Sensor...ok");
    u8g2.drawStr(0, 30, "Bluetooth....aktiviert");
    u8g2.drawStr(0, 40, "WiFi...verbunden");
    //u8g2.drawStr(0, 50, "IP:" + WiFi.localIP());
  } while (u8g2.nextPage());
  delay(1000);
  /*config time for date based led */
  configTime(timezone, dst, "pool.ntp.org","time.nist.gov");
  getWeatherData();
  showLEDs();
  /*configure pump*/
  pinMode(PUMP_PIN, OUTPUT);
  digitalWrite(PUMP_PIN,LOW);//default off
  SerialBT.begin("ESP32");
  Serial.print("setup finished");
}// end setup

void loop()
{
  /*update weather all 5min*/
   unsigned long currentMillis = millis();
   if (millis() - lastCallTime > postingInterval) {
      lastCallTime = currentMillis;
      SerialBT.end(); // we dont want porblems woth the WiFi-Gang :D
      getWeatherData(); // get weather data all 10min
      showLEDs();
  /*plant watering check all */
  //   handlePlant();
      drawWeatherPage();
      // WiFi.mode(WIFI_OFF);
      SerialBT.begin("ESP32");// Here BT is again :D
    }
  /*update pages */
  unsigned long currentMillis_page = millis();
   if (millis() - lastCallTime_page > postingInterval_page)
   {
      lastCallTime_page = currentMillis_page;
      
      switch (page_cycles)
      {
      case 0: // 10s
        page_cycles +=1;
        drawBME(); // Values of BME280
        break;
      case 1: //20s
        page_cycles +=1;
        drawWeatherPage(); // weather data
        break;
      case 2: // 30s
        page_cycles = 0;
        //  drawPlant();
        break;
      default:
       page_cycles = 0;
        break;
      }
   }
  if(SerialBT.hasClient()) getblecmd(&SerialBT);

}

/**
 * @brief get WiFi credentials via bluetooth
 * 
 * @param ssid_bl 
 * @param pw_bl 
 * @param bt_handler 
 */
void bl_get_WiFi(char *ssid_bl, char *pw_bl,BluetoothSerial* bt_handler){
  int c_ssid = 0; //1 = got ssid
  int c_pw = 0; //1 = got pw
  String bl_message = "";
  /*get msg*/
  while((c_ssid==0) || (c_pw==0)){
    if (bt_handler->available()){ //coming in in chars not as String!
      char incomingChar = bt_handler->read();
      if (incomingChar != '\n'){
        bl_message += String(incomingChar);
      }
      else{
        bl_message = "";
      }
    }
    delay(20);
    /*get ssid*/
    /***************************&& (c_ssid==0) raus***********/
    if(bl_message.endsWith(":ssid") && (c_ssid==0)){ // from end cause bl_serial receive evry char. So marked needs to be at the end
      bl_message.replace(":ssid",""); //delete maker
      bl_message.toCharArray(ssid_bl,bl_message.length()+1); //pointer to ssid_buff var
      c_ssid = 1; // received ssid
      bl_message = "";
    }//end get ssid
    /*get pw*/
    if(bl_message.endsWith(":pw")&& (c_pw==0)){ // from end cause bl_serial receive evry char. So marked needs to be at the end
      bl_message.replace(":pw","");//delete maker
      bl_message.toCharArray(pw_bl,bl_message.length()+1); //pointer to ssid_buff var
      c_pw = 1; //received pw
      bl_message = "";
    }//end pw
  }//end While
}//end bl_get_WiFi

/**
 * @brief get and handle bluetooth
 * 
 * @param SerialBT 
 */

void getblecmd(BluetoothSerial* bt_handler){
  /*get msg*/
  if (bt_handler->available()){
    char incomingChar = bt_handler->read();
    if (incomingChar != '\n')
    {
      message += String(incomingChar);
    }
    else
    {
      message = "";
    } 
  }
  delay(20);

  /*set RGB*/
  if(message.endsWith(":rgb")){// from end cause bl_serial receive evry char. So marked needs to be at the end
    message.replace(":rgb",""); //delete maker
    char rgb[12] = ""; // assume this string is result read from serial
    message.toCharArray(rgb,message.length()+1); //store values in charArray
    int r, g, b;
    Serial.print("rgb:");Serial.println(message);
    if (sscanf(rgb, "%d,%d,%d", &r, &g, &b) == 3)
    {
      for(int i=0;i<NUM_LEDS-1;i++)leds[i].setRGB(r, g, b);
      FastLED.show();
    }
  }
  /*pump controll with a REAL delay*/
  if(message.endsWith(":pump")){
    message.replace(":pump",""); //delete maker
    int t_pump_on = message.toInt();
    if(t_pump_on<PUMP_MAX_ON){// max 
      digitalWrite(PUMP_PIN,HIGH);//on
      delay(t_pump_on);
      digitalWrite(PUMP_PIN,LOW);//off 
    }
  }//end Pump


}
/**
 * @brief 
 * 
 */
void connectToWifi()
{
  WiFi.enableSTA(true);
  delay(2000);

  WiFi.begin(ssid,password);
  Serial.println("Connecting to network");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }

  Serial.println(WiFi.localIP());
}

String httpGETRequest(const char *serverName)
{
  HTTPClient http;

  // Your IP address with path or Domain name with URL path
  http.begin(serverName);

  // Send HTTP POST request
  int httpResponseCode = http.GET();

  String payload = "{}";

  if (httpResponseCode > 0)
  {
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);
    payload = http.getString();
  }
  else
  {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
  }
  // Free resources
  http.end();

  return payload;
}

void getWeatherData() //client function to send/receive GET request data.
{
  String jsonBuffer;
  String serverPath = "http://api.openweathermap.org/data/2.5/weather?q=Hamburg&APPID=9fc2f6d9361da88c6587b261b927a8de";
  jsonBuffer = httpGETRequest(serverPath.c_str());
  Serial.println(jsonBuffer);
  result.replace('[', ' ');
  result.replace(']', ' ');

  char jsonArray[result.length() + 1];
  result.toCharArray(jsonArray, sizeof(jsonArray));
  jsonArray[result.length() + 1] = '\0';

  // StaticJsonDocument<1024> doc;
  StaticJsonDocument<1024> doc;
  DeserializationError error = deserializeJson(doc, jsonBuffer);
  //JsonObject &root = json_buf.parseObject(jsonArray);
  if (error)
  {
    Serial.print(F("deserializeJson() failed with code "));
    Serial.println(error.c_str());
    return;
  }
  float kelvin = -272.15;
  Serial.println(jsonArray);
  /*data into typedef*/
  current_data_t.weatherId = doc["weather"][0]["id"].as<uint16_t>(); // "id": 521,
  current_data_t.main = (const char *)doc["weather"]["main"];        // "main": "Rain",
  current_data_t.description = (const char *)doc["weather"][0]["description"];
  current_data_t.icon = (const char *)doc["weather"][0]["icon"];

  current_data_t.temp = (doc["main"]["temp"].as<float>() + kelvin);
  current_data_t.pressure = doc["main"]["pressure"].as<uint16_t>();
  current_data_t.humidity = doc["main"]["humidity"].as<uint8_t>();
  current_data_t.tempMin = (doc["main"]["temp_min"].as<float>() + kelvin);
  current_data_t.tempMax = (doc["main"]["temp_max"].as<float>() + kelvin);
  current_data_t.visibility = doc["visibility"];
  current_data_t.windSpeed = doc["wind"]["speed"].as<float>();
  current_data_t.windDeg = doc["wind"]["deg"].as<float>();
  current_data_t.clouds = doc["clouds"]["all"].as<uint8_t>();
  current_data_t.country = (const char *)doc["sys"][2];
  current_data_t.sunrise = doc["sys"]["sunrise"];
  current_data_t.sunset = doc["sys"]["sunset"];
  current_data_t.cityName = (const char *)doc["name"];
}
/**
 * @brief 
 * 
 * @param weatherID 
 */
void drawWeatherPage()
{
  //---------------------Pic symbol-----------------------------//
  const uint8_t *symbol;
  switch (current_data_t.weatherId)
  {
  case 800:
    symbol = CLEARSKY;
    break;
  case 801:
    symbol = FEWCLOUDS;
    break;
  case 802:
    symbol = FEWCLOUDS;
    break;
  case 803:
    symbol = SCATTEREDCLOUDS;
    break;
  case 804:
    symbol = SCATTEREDCLOUDS;
    break;

  case 200:
    symbol = THUNDERSTORM;
    break;
  case 201:
    symbol = THUNDERSTORM;
    break;
  case 202:
    symbol = THUNDERSTORM;
    break;
  case 210:
    symbol = THUNDERSTORM;
    break;
  case 211:
    symbol = THUNDERSTORM;
    break;
  case 212:
    symbol = THUNDERSTORM;
    break;
  case 221:
    symbol = THUNDERSTORM;
    break;
  case 230:
    symbol = THUNDERSTORM;
    break;
  case 231:
    symbol = THUNDERSTORM;
    break;
  case 232:
    symbol = THUNDERSTORM;
    break;

    // case 300: drawLightRain(; break;
    // case 301: drawLightRain(; break;
    // case 302: drawLightRain(; break;
    // case 310: drawLightRain(; break;
    // case 311: drawLightRain(; break;
    // case 312: drawLightRain(; break;
    // case 313: drawLightRain(; break;
    // case 314: drawLightRain(; break;
    // case 321: drawLightRain(; break;

    // case 500: drawLightRainWithSunOrMoon(; break;
    // case 501: drawLightRainWithSunOrMoon(; break;
    // case 502: drawLightRainWithSunOrMoon(; break;
    // case 503: drawLightRainWithSunOrMoon(; break;
    // case 504: drawLightRainWithSunOrMoon(; break;
    // case 511: drawLightRain(; break;
    // case 520: drawModerateRain(; break;
    // case 521: drawModerateRain(; break;
    // case 522: drawHeavyRain(; break;
    // case 531: drawHeavyRain(; break;

  case 600:
    symbol = SNOWCLOUDS;
    break;
  case 601:
    symbol = SNOWCLOUDS;
    break;
  case 602:
    symbol = SNOWCLOUDS;
    break;
  case 611:
    symbol = SNOWCLOUDS;
    break;
  case 612:
    symbol = SNOWCLOUDS;
    break;
  case 615:
    symbol = SNOWCLOUDS;
    break;
  case 616:
    symbol = SNOWCLOUDS;
    break;
  case 620:
    symbol = SNOWCLOUDS;
    break;
  case 621:
    symbol = SNOWCLOUDS;
    break;
  case 622:
    symbol = SNOWCLOUDS;
    break;

  case 701:
    symbol = FOGCLOUD;
    break;
  case 711:
    symbol = FOGCLOUD;
    break;
  case 721:
    symbol = FOGCLOUD;
    break;
  case 731:
    symbol = FOGCLOUD;
    break;
  case 741:
    symbol = FOGCLOUD;
    break;
  case 751:
    symbol = FOGCLOUD;
    break;
  case 761:
    symbol = FOGCLOUD;
    break;
  case 762:
    symbol = FOGCLOUD;
    break;
  case 771:
    symbol = FOGCLOUD;
    break;
  case 781:
    symbol = FOGCLOUD;
    break;

  default:
    break;
  } //end switch case

  u8g2.firstPage();
  do
  {
    //draw symbols
    u8g2.setDrawColor(0);                //inverted
    u8g2.drawXBMP(0, 0, 64, 64, symbol); //weather symbol
    // u8g2.drawXBMP(offset_x, 0,24,24, WINDSYMBOL); //wind symbol
    // u8g2.drawXBMP(offset_x, 24,24,24, PRESSURESYMBOL); // pressure symbol
    // u8g2.drawXBMP(offset_x, 48,24,24, TEMPSYMBOL); // temp symbol
    char buffer[20];
    u8g2.setDrawColor(1);              // not inverted
    u8g2.setFont(u8g2_font_luRS08_te); // choose a suitable font
                                       // windspeed m/s
    sprintf(buffer, "%.1f m/s\n", current_data_t.windSpeed);
    u8g2.drawStr(70, 10, buffer);
    // pressure hPa
    sprintf(buffer, "%d hPa\n", current_data_t.pressure);
    u8g2.drawStr(70, 26, buffer);
    // temperature °C
    sprintf(buffer, "%.1f *C\n", current_data_t.temp);
    u8g2.drawStr(70, 42, buffer);
    // humidity %
    sprintf(buffer, "%.d %% \n", current_data_t.humidity);
    u8g2.drawStr(70, 58, buffer);

  } while (u8g2.nextPage()); //end if page is drawn
}
/**
 * @brief get BEM280 data and print to SSD1306
 * 
 */
void drawBME()
{

  float bme_p = (bme.readPressure() / 100.0F);
  float bme_a = bme.readAltitude(SEALEVELPRESSURE_HPA);
  float bme_t = bme.readTemperature();
  float bme_h = bme.readHumidity();

  u8g2.firstPage();
  do
  {

    u8g2.setDrawColor(1);              // not inverted
    u8g2.setFont(u8g2_font_luRS08_te); // choose a suitable font
    // temperature °C
    char buffer[20];
    sprintf(buffer, "Temp: %.1f *C\n", bme_t);
    u8g2.drawStr(10, 10, buffer);
    // pressure hPa
    sprintf(buffer, "Druck: %.1f hPa\n", bme_p);
    u8g2.drawStr(10, 26, buffer);
    // altiude m
    sprintf(buffer, "Hoehe: %.1f m\n", bme_a);
    u8g2.drawStr(10, 42, buffer);
    // humidity %
    sprintf(buffer, "Feuchte: %.1f %% \n", bme_h);
    u8g2.drawStr(10, 58, buffer);

  } while (u8g2.nextPage()); //end if page is drawn
}

void initLED(){
    FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
    FastLED.setBrightness(  BRIGHTNESS );
}

void showLEDs(){

  time_t now = time(nullptr);
  struct tm* p_tm = localtime(&now);
  Serial.print("-------------------------------------------------\n");
  Serial.print("Date & Time : ");
  Serial.print(p_tm->tm_mday);
  Serial.print("/");
  Serial.print(p_tm->tm_mon + 1);
  Serial.print("/");
  Serial.print(p_tm->tm_year + 1900);
  Serial.print(" ");
  Serial.print(p_tm->tm_hour);
  Serial.print(":");
  Serial.print(p_tm->tm_min);
  Serial.print(":");
  Serial.println(p_tm->tm_sec);
  Serial.print("main: ");
  Serial.println(current_data_t.main.c_str());
  Serial.printf("description: ");
  Serial.println(current_data_t.description.c_str());
  Serial.printf("temp: ");
  Serial.println(current_data_t.temp);
  Serial.printf("clouds: ");
  Serial.println(current_data_t.clouds);
  Serial.print("-------------------------------------------------\n");

  String c=current_data_t.description.c_str(); 
  int h=p_tm->tm_hour;
  int temp=current_data_t.temp;
  /*------------------------Morning-------------------------*/
  if(h >= 5 && h < 9 && (c == "clear sky" || c == "few clouds"))
  {
   leds[24].setRGB(255, 47, 0); 
   leds[23].setRGB(36, 0, 16);
   
   leds[16].setRGB(0,0,0); 
   leds[17].setRGB(0,0,0); 
   leds[18].setRGB(0,0,0); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(0,0,0); 
   leds[21].setRGB(0,0,0);
   leds[22].setRGB(0,0,0);
   FastLED.show();
   delay(5000);
  }
  if(h >= 5 && h < 9 && c == "scattered clouds")
  {
   leds[24].setRGB(255, 47, 0); 
   leds[23].setRGB(36, 0, 16); 

   leds[16].setRGB(5, 173, 245); 
   leds[17].setRGB(245, 36, 5); 
   leds[18].setRGB(245, 36, 5); 
   leds[19].setRGB(245, 36, 5);
   leds[20].setRGB(245, 36, 5); 
   leds[21].setRGB(245, 36, 5);
   leds[22].setRGB(245, 36, 5);
   FastLED.show();
   delay(5000);
  }
  if(h >= 5 && h < 9 && (c == "broken clouds" || c == "overcast clouds"))
  {
   leds[16].setRGB(24, 82, 89); 
   leds[17].setRGB(24, 82, 89); 
   leds[18].setRGB(104, 0, 214); 
   leds[19].setRGB(104, 0, 214);
   leds[20].setRGB(104, 0, 214); 
   leds[21].setRGB(99, 12, 6);
   leds[22].setRGB(99, 12, 6);
   
   leds[24].setRGB(255, 47, 0); 
   leds[23].setRGB(36, 0, 16); 
   FastLED.show();
   delay(5000);
  }
  if(h >= 5 && h<9 && (c == "light rain" || c == "moderate rain" || c == "heavy intensity rain"))
  {
   leds[24].setRGB(255, 47, 0); 
   leds[23].setRGB(36, 0, 16);
   FastLED.show();
   
   leds[16].setRGB(59, 0, 79); 
   leds[17].setRGB(59, 0, 79); 
   leds[18].setRGB(59, 0, 79); 
   leds[19].setRGB(59, 0, 79);
   leds[20].setRGB(59, 0, 79); 
   leds[21].setRGB(59, 0, 79);
   leds[22].setRGB(59, 0, 79);
   FastLED.show();
   delay(50);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(59, 0, 79); 
   leds[17].setRGB(59, 0, 79); 
   leds[18].setRGB(59, 0, 79); 
   leds[19].setRGB(59, 0, 79);
   leds[20].setRGB(59, 0, 79); 
   leds[21].setRGB(59, 0, 79);
   leds[22].setRGB(59, 0, 79);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(59, 0, 79); 
   leds[17].setRGB(59, 0, 79); 
   leds[18].setRGB(59, 0, 79); 
   leds[19].setRGB(59, 0, 79);
   leds[20].setRGB(59, 0, 79); 
   leds[21].setRGB(59, 0, 79);
   leds[22].setRGB(59, 0, 79);
   FastLED.show();
   delay(50);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(59, 0, 79); 
   leds[17].setRGB(59, 0, 79); 
   leds[18].setRGB(59, 0, 79); 
   leds[19].setRGB(59, 0, 79);
   leds[20].setRGB(59, 0, 79); 
   leds[21].setRGB(59, 0, 79);
   leds[22].setRGB(59, 0, 79);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(59, 0, 79); 
   leds[17].setRGB(59, 0, 79); 
   leds[18].setRGB(59, 0, 79); 
   leds[19].setRGB(59, 0, 79);
   leds[20].setRGB(59, 0, 79); 
   leds[21].setRGB(59, 0, 79);
   leds[22].setRGB(59, 0, 79);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(59, 0, 79); 
   leds[17].setRGB(59, 0, 79); 
   leds[18].setRGB(59, 0, 79); 
   leds[19].setRGB(59, 0, 79);
   leds[20].setRGB(59, 0, 79); 
   leds[21].setRGB(59, 0, 79);
   leds[22].setRGB(59, 0, 79);
   FastLED.show();
   delay(50);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(59, 0, 79); 
   leds[17].setRGB(59, 0, 79); 
   leds[18].setRGB(59, 0, 79); 
   leds[19].setRGB(59, 0, 79);
   leds[20].setRGB(59, 0, 79); 
   leds[21].setRGB(59, 0, 79);
   leds[22].setRGB(59, 0, 79);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(59, 0, 79); 
   leds[17].setRGB(59, 0, 79); 
   leds[18].setRGB(59, 0, 79); 
   leds[19].setRGB(59, 0, 79);
   leds[20].setRGB(59, 0, 79); 
   leds[21].setRGB(59, 0, 79);
   leds[22].setRGB(59, 0, 79);
   FastLED.show();
   delay(50);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(59, 0, 79); 
   leds[17].setRGB(59, 0, 79); 
   leds[18].setRGB(59, 0, 79); 
   leds[19].setRGB(59, 0, 79);
   leds[20].setRGB(59, 0, 79); 
   leds[21].setRGB(59, 0, 79);
   leds[22].setRGB(59, 0, 79);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(59, 0, 79); 
   leds[17].setRGB(59, 0, 79); 
   leds[18].setRGB(59, 0, 79); 
   leds[19].setRGB(59, 0, 79);
   leds[20].setRGB(59, 0, 79); 
   leds[21].setRGB(59, 0, 79);
   leds[22].setRGB(59, 0, 79);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(59, 0, 79); 
   leds[17].setRGB(59, 0, 79); 
   leds[18].setRGB(59, 0, 79); 
   leds[19].setRGB(59, 0, 79);
   leds[20].setRGB(59, 0, 79); 
   leds[21].setRGB(59, 0, 79);
   leds[22].setRGB(59, 0, 79);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(59, 0, 79); 
   leds[17].setRGB(59, 0, 79); 
   leds[18].setRGB(59, 0, 79); 
   leds[19].setRGB(59, 0, 79);
   leds[20].setRGB(59, 0, 79); 
   leds[21].setRGB(59, 0, 79);
   leds[22].setRGB(59, 0, 79);
   delay(200);
  }


  /*------------------------Afternoon-------------------------*/
  if(h >= 9 && h < 17 && (c == "clear sky" || c == "few clouds"))
  {
   leds[23].setRGB(255, 47, 0); 
   leds[24].setRGB(255, 42, 0);
   
   leds[16].setRGB(245, 187, 93); 
   leds[17].setRGB(245, 187, 93); 
   leds[18].setRGB(245, 187, 93); 
   leds[19].setRGB(245, 187, 93);
   leds[20].setRGB(245, 187, 93); 
   leds[21].setRGB(245, 187, 93);
   leds[22].setRGB(245, 187, 93);
   FastLED.show();
   delay(5000);
  }
  if(h >= 9 && h < 17 && c == "scattered clouds")
  {
   leds[16].setRGB(255, 0, 0); 
   leds[17].setRGB(255, 0, 0); 
   leds[18].setRGB(230, 252, 255); 
   leds[19].setRGB(230, 252, 255);
   leds[20].setRGB(230, 252, 255); 
   leds[21].setRGB(230, 252, 255);
   leds[22].setRGB(230, 252, 255);
   
   leds[23].setRGB(255, 47, 0); 
   leds[24].setRGB(255, 42, 0);
    FastLED.show();
    delay(5000);
  }
  if(h >= 9 && h <17 && (c == "broken clouds" || c == "overcast clouds"))
  {
   leds[16].setRGB(24, 82, 89); 
   leds[17].setRGB(24, 82, 89); 
   leds[18].setRGB(24, 82, 89); 
   leds[19].setRGB(41, 8, 69);
   leds[20].setRGB(41, 8, 69); 
   leds[21].setRGB(99, 12, 6);
   leds[22].setRGB(99, 12, 6);
   
   leds[23].setRGB(255, 47, 0); 
   leds[24].setRGB(255, 42, 0);
    FastLED.show();
    delay(5000);
  }
  if(h >= 9 && h < 17 && (c == "light rain" || c == "moderate rain" || c == "heavy intensity rain"))
  {
   leds[23].setRGB(255, 47, 0); 
   leds[24].setRGB(255, 42, 0);
    FastLED.show();

   leds[16].setRGB(0,0,0); 
   leds[17].setRGB(0,0,0); 
   leds[18].setRGB(59, 44, 36); 
   leds[19].setRGB(59, 44, 36);
   leds[20].setRGB(59, 44, 36); 
   leds[21].setRGB(59, 44, 36);
   leds[22].setRGB(59, 44, 36);
   FastLED.show();
   delay(50);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(0,0,0); 
   leds[17].setRGB(0,0,0); 
   leds[18].setRGB(59, 44, 36); 
   leds[19].setRGB(59, 44, 36);
   leds[20].setRGB(59, 44, 36); 
   leds[21].setRGB(59, 44, 36);
   leds[22].setRGB(59, 44, 36);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(0,0,0); 
   leds[17].setRGB(0,0,0); 
   leds[18].setRGB(59, 44, 36); 
   leds[19].setRGB(59, 44, 36);
   leds[20].setRGB(59, 44, 36); 
   leds[21].setRGB(59, 44, 36);
   leds[22].setRGB(59, 44, 36);
   FastLED.show();
   delay(50);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(0,0,0); 
   leds[17].setRGB(0,0,0); 
   leds[18].setRGB(59, 44, 36); 
   leds[19].setRGB(59, 44, 36);
   leds[20].setRGB(59, 44, 36); 
   leds[21].setRGB(59, 44, 36);
   leds[22].setRGB(59, 44, 36);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(0,0,0); 
   leds[17].setRGB(0,0,0); 
   leds[18].setRGB(59, 44, 36); 
   leds[19].setRGB(59, 44, 36);
   leds[20].setRGB(59, 44, 36); 
   leds[21].setRGB(59, 44, 36);
   leds[22].setRGB(59, 44, 36);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(0,0,0); 
   leds[17].setRGB(0,0,0); 
   leds[18].setRGB(59, 44, 36); 
   leds[19].setRGB(59, 44, 36);
   leds[20].setRGB(59, 44, 36); 
   leds[21].setRGB(59, 44, 36);
   leds[22].setRGB(59, 44, 36);
   FastLED.show();
   delay(50);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(0,0,0); 
   leds[17].setRGB(0,0,0); 
   leds[18].setRGB(59, 44, 36); 
   leds[19].setRGB(59, 44, 36);
   leds[20].setRGB(59, 44, 36); 
   leds[21].setRGB(59, 44, 36);
   leds[22].setRGB(59, 44, 36);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(0,0,0); 
   leds[17].setRGB(0,0,0); 
   leds[18].setRGB(59, 44, 36); 
   leds[19].setRGB(59, 44, 36);
   leds[20].setRGB(59, 44, 36); 
   leds[21].setRGB(59, 44, 36);
   leds[22].setRGB(59, 44, 36);
   FastLED.show();
   delay(50);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(0,0,0); 
   leds[17].setRGB(0,0,0); 
   leds[18].setRGB(59, 44, 36); 
   leds[19].setRGB(59, 44, 36);
   leds[20].setRGB(59, 44, 36); 
   leds[21].setRGB(59, 44, 36);
   leds[22].setRGB(59, 44, 36);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(0,0,0); 
   leds[17].setRGB(0,0,0); 
   leds[18].setRGB(59, 44, 36); 
   leds[19].setRGB(59, 44, 36);
   leds[20].setRGB(59, 44, 36); 
   leds[21].setRGB(59, 44, 36);
   leds[22].setRGB(59, 44, 36);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(0,0,0); 
   leds[17].setRGB(0,0,0); 
   leds[18].setRGB(59, 44, 36); 
   leds[19].setRGB(59, 44, 36);
   leds[20].setRGB(59, 44, 36); 
   leds[21].setRGB(59, 44, 36);
   leds[22].setRGB(59, 44, 36);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
  }

  /*------------------------Evening-------------------------*/
  if(h >= 17 && h < 19 && (c == "clear sky" || c == "few clouds"))
  {
   leds[23].setRGB(255, 47, 0); 
   leds[24].setRGB(71, 42, 10);
   
   leds[16].setRGB(66, 16, 65); 
   leds[17].setRGB(66, 16, 65); 
   leds[18].setRGB(245, 36, 5); 
   leds[19].setRGB(245, 36, 5);
   leds[20].setRGB(245, 36, 5); 
   leds[21].setRGB(245, 36, 5);
   leds[22].setRGB(245, 36, 5);
   FastLED.show();
   delay(5000);
  }
  if(h >= 17 && h < 19 && c == "scattered clouds")
  {
   leds[23].setRGB(255, 47, 0); 
   leds[24].setRGB(71, 42, 10);
   FastLED.show();
   
   leds[16].setRGB(245, 36, 5); 
   leds[17].setRGB(245, 36, 5); 
   leds[18].setRGB(245, 36, 5); 
   leds[19].setRGB(0, 89, 110);
   leds[20].setRGB(245, 36, 5); 
   leds[21].setRGB(245, 36, 5);
   leds[22].setRGB(245, 36, 5);
   FastLED.show();
   delay(5000);
  }
  if(h >= 17 && h < 19 && (c == "broken clouds" || c == "overcast clouds"))
  {
   leds[23].setRGB(255, 47, 0); 
   leds[24].setRGB(71, 42, 10);
   
   leds[16].setRGB(245, 36, 5); 
   leds[17].setRGB(245, 36, 5); 
   leds[18].setRGB(66, 16, 65); 
   leds[19].setRGB(66, 16, 65);
   leds[20].setRGB(66, 16, 65); 
   leds[21].setRGB(66, 16, 65);
   leds[22].setRGB(66, 16, 65);
   FastLED.show(); 
   delay(5000);
  }
  if(h >= 17 && h < 19 && (c == "light rain" || c == "moderate rain" || c == "heavy intensity rain"))
  {
   leds[23].setRGB(255, 47, 0); 
   leds[24].setRGB(71, 42, 10); 
   FastLED.show(); 

   leds[16].setRGB(46, 15, 0); 
   leds[17].setRGB(46, 15, 0); 
   leds[18].setRGB(46, 15, 0); 
   leds[19].setRGB(46, 15, 0);
   leds[20].setRGB(46, 15, 0); 
   leds[21].setRGB(46, 15, 0);
   leds[22].setRGB(46, 15, 0);
   FastLED.show();
   delay(50);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(46, 15, 0); 
   leds[17].setRGB(46, 15, 0); 
   leds[18].setRGB(46, 15, 0); 
   leds[19].setRGB(46, 15, 0);
   leds[20].setRGB(46, 15, 0); 
   leds[21].setRGB(46, 15, 0);
   leds[22].setRGB(46, 15, 0);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(46, 15, 0); 
   leds[17].setRGB(46, 15, 0); 
   leds[18].setRGB(46, 15, 0); 
   leds[19].setRGB(46, 15, 0);
   leds[20].setRGB(46, 15, 0); 
   leds[21].setRGB(46, 15, 0);
   leds[22].setRGB(46, 15, 0);
   FastLED.show();
   delay(50);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(46, 15, 0); 
   leds[17].setRGB(46, 15, 0); 
   leds[18].setRGB(46, 15, 0); 
   leds[19].setRGB(46, 15, 0);
   leds[20].setRGB(46, 15, 0); 
   leds[21].setRGB(46, 15, 0);
   leds[22].setRGB(46, 15, 0);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(46, 15, 0); 
   leds[17].setRGB(46, 15, 0); 
   leds[18].setRGB(46, 15, 0); 
   leds[19].setRGB(46, 15, 0);
   leds[20].setRGB(46, 15, 0); 
   leds[21].setRGB(46, 15, 0);
   leds[22].setRGB(46, 15, 0);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(46, 15, 0); 
   leds[17].setRGB(46, 15, 0); 
   leds[18].setRGB(46, 15, 0); 
   leds[19].setRGB(46, 15, 0);
   leds[20].setRGB(46, 15, 0); 
   leds[21].setRGB(46, 15, 0);
   leds[22].setRGB(46, 15, 0);
   FastLED.show();
   delay(50);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(46, 15, 0); 
   leds[17].setRGB(46, 15, 0); 
   leds[18].setRGB(46, 15, 0); 
   leds[19].setRGB(46, 15, 0);
   leds[20].setRGB(46, 15, 0); 
   leds[21].setRGB(46, 15, 0);
   leds[22].setRGB(46, 15, 0);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(46, 15, 0); 
   leds[17].setRGB(46, 15, 0); 
   leds[18].setRGB(46, 15, 0); 
   leds[19].setRGB(46, 15, 0);
   leds[20].setRGB(46, 15, 0); 
   leds[21].setRGB(46, 15, 0);
   leds[22].setRGB(46, 15, 0);
   FastLED.show();
   delay(50);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(46, 15, 0); 
   leds[17].setRGB(46, 15, 0); 
   leds[18].setRGB(46, 15, 0); 
   leds[19].setRGB(46, 15, 0);
   leds[20].setRGB(46, 15, 0); 
   leds[21].setRGB(46, 15, 0);
   leds[22].setRGB(46, 15, 0);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(46, 15, 0); 
   leds[17].setRGB(46, 15, 0); 
   leds[18].setRGB(46, 15, 0); 
   leds[19].setRGB(46, 15, 0);
   leds[20].setRGB(46, 15, 0); 
   leds[21].setRGB(46, 15, 0);
   leds[22].setRGB(46, 15, 0);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(46, 15, 0); 
   leds[17].setRGB(46, 15, 0); 
   leds[18].setRGB(46, 15, 0); 
   leds[19].setRGB(46, 15, 0);
   leds[20].setRGB(46, 15, 0); 
   leds[21].setRGB(46, 15, 0);
   leds[22].setRGB(46, 15, 0);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(46, 15, 0); 
   leds[17].setRGB(46, 15, 0); 
   leds[18].setRGB(46, 15, 0); 
   leds[19].setRGB(46, 15, 0);
   leds[20].setRGB(46, 15, 0); 
   leds[21].setRGB(46, 15, 0);
   leds[22].setRGB(46, 15, 0);
   FastLED.show();
   delay(200);
  }

  /*------------------------Night-------------------------*/
  if((h >= 19 || h < 5)&& (c == "clear sky" || c == "few clouds"))
  {
   leds[23].setRGB(232, 224, 216); 
   leds[24].setRGB(61, 51, 48); 
   
   leds[16].setRGB(0,0,0); 
   leds[17].setRGB(0,0,0); 
   leds[18].setRGB(20, 27, 31); 
   leds[19].setRGB(20, 27, 31);
   leds[20].setRGB(40, 51, 56); 
   leds[21].setRGB(40, 51, 56);
   leds[21].setRGB(40, 51, 56);
   FastLED.show();
   delay(5000);
  }
  if((h >= 19 || h < 5)&& c == "scattered clouds")
  {
   leds[23].setRGB(232, 224, 216); 
   leds[24].setRGB(61, 51, 48); 
   
   leds[16].setRGB(0,0,0); 
   leds[17].setRGB(0,0,0); 
   leds[18].setRGB(16, 46, 71); 
   leds[19].setRGB(16, 46, 71);
   leds[20].setRGB(16, 46, 71); 
   leds[21].setRGB(40, 51, 56);
   leds[22].setRGB(40, 51, 56);
   FastLED.show();
   delay(5000);
  }
  if((h >= 19 || h < 5) && (c == "broken clouds" || c == "overcast clouds"))
  {
   leds[23].setRGB(232, 224, 216); 
   leds[24].setRGB(61, 51, 48); 
   leds[16].setRGB(0,0,0); 
   leds[17].setRGB(0,0,0); 
   leds[18].setRGB(4, 12, 18); 
   leds[19].setRGB(4, 12, 18);
   leds[20].setRGB(4, 12, 18); 
   leds[21].setRGB(4, 12, 18);
   leds[22].setRGB(40, 51, 56);
   FastLED.show();
   delay(5000);
  }
  if((h >= 19 || h < 5) && (c == "light rain" || c == "moderate rain" || c == "heavy intensity rain"))
  {
   leds[23].setRGB(232, 224, 216); 
   leds[24].setRGB(61, 51, 48); 
    FastLED.show();

   leds[16].setRGB(0,0,0); 
   leds[17].setRGB(0,0,0); 
   leds[18].setRGB(0,0,0); 
   leds[19].setRGB(0, 17, 20);
   leds[20].setRGB(0, 17, 20); 
   leds[21].setRGB(0, 17, 20);
   leds[22].setRGB(0, 17, 20);
   FastLED.show();
   delay(50);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(0,0,0); 
   leds[17].setRGB(0,0,0); 
   leds[18].setRGB(0,0,0); 
   leds[19].setRGB(0, 17, 20);
   leds[20].setRGB(0, 17, 20); 
   leds[21].setRGB(0, 17, 20);
   leds[22].setRGB(0, 17, 20);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(0,0,0); 
   leds[17].setRGB(0,0,0); 
   leds[18].setRGB(0,0,0); 
   leds[19].setRGB(0, 17, 20);
   leds[20].setRGB(0, 17, 20); 
   leds[21].setRGB(0, 17, 20);
   leds[22].setRGB(0, 17, 20);
   FastLED.show();
   delay(50);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(0,0,0); 
   leds[17].setRGB(0,0,0); 
   leds[18].setRGB(0,0,0); 
   leds[19].setRGB(0, 17, 20);
   leds[20].setRGB(0, 17, 20); 
   leds[21].setRGB(0, 17, 20);
   leds[22].setRGB(0, 17, 20);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(0,0,0); 
   leds[17].setRGB(0,0,0); 
   leds[18].setRGB(0,0,0); 
   leds[19].setRGB(0, 17, 20);
   leds[20].setRGB(0, 17, 20); 
   leds[21].setRGB(0, 17, 20);
   leds[22].setRGB(0, 17, 20);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(0,0,0); 
   leds[17].setRGB(0,0,0); 
   leds[18].setRGB(0,0,0); 
   leds[19].setRGB(0, 17, 20);
   leds[20].setRGB(0, 17, 20); 
   leds[21].setRGB(0, 17, 20);
   leds[22].setRGB(0, 17, 20);
   FastLED.show();
   delay(50);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(0,0,0); 
   leds[17].setRGB(0,0,0); 
   leds[18].setRGB(0,0,0); 
   leds[19].setRGB(0, 17, 20);
   leds[20].setRGB(0, 17, 20); 
   leds[21].setRGB(0, 17, 20);
   leds[22].setRGB(0, 17, 20);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(0,0,0); 
   leds[17].setRGB(0,0,0); 
   leds[18].setRGB(0,0,0); 
   leds[19].setRGB(0, 17, 20);
   leds[20].setRGB(0, 17, 20); 
   leds[21].setRGB(0, 17, 20);
   leds[22].setRGB(0, 17, 20);
   FastLED.show();
   delay(50);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(0,0,0); 
   leds[17].setRGB(0,0,0); 
   leds[18].setRGB(0,0,0); 
   leds[19].setRGB(0, 17, 20);
   leds[20].setRGB(0, 17, 20); 
   leds[21].setRGB(0, 17, 20);
   leds[22].setRGB(0, 17, 20);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(0,0,0); 
   leds[17].setRGB(0,0,0); 
   leds[18].setRGB(0,0,0); 
   leds[19].setRGB(0, 17, 20);
   leds[20].setRGB(0, 17, 20); 
   leds[21].setRGB(0, 17, 20);
   leds[22].setRGB(0, 17, 20);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
   leds[16].setRGB(0,0,0); 
   leds[17].setRGB(0,0,0); 
   leds[18].setRGB(0,0,0); 
   leds[19].setRGB(0, 17, 20);
   leds[20].setRGB(0, 17, 20); 
   leds[21].setRGB(0, 17, 20);
   leds[22].setRGB(0, 17, 20);
   FastLED.show();
   delay(500);
   leds[16].setRGB(255, 255, 255); 
   leds[17].setRGB(255, 255, 255); 
   leds[18].setRGB(255, 255, 255); 
   leds[19].setRGB(255, 255, 255);
   leds[20].setRGB(255, 255, 255); 
   leds[21].setRGB(255, 255, 255);
   leds[22].setRGB(255, 255, 255);
   FastLED.show();
   delay(100);
  }

  /*-----------------------Temperature-------------------------*/
  if(temp<20)
  {
    leds[0].setRGB(252, 163, 28);
    leds[1].setRGB(252, 163, 28);
    leds[2].setRGB(252, 163, 28);
    leds[3].setRGB(252, 163, 28);
    leds[4].setRGB(252, 163, 28);
    leds[5].setRGB(252, 163, 28);
    leds[6].setRGB(252, 163, 28);
    leds[7].setRGB(252, 163, 28);
    leds[8].setRGB(252, 163, 28);
    leds[9].setRGB(252, 163, 28);
    leds[10].setRGB(252, 163, 28);
    leds[11].setRGB(252, 163, 28);
    leds[12].setRGB(252, 163, 28);
    leds[13].setRGB(252, 163, 28);
    leds[14].setRGB(252, 163, 28);
    leds[15].setRGB(252, 163, 28);
    FastLED.show();
  }
  if(temp>=20 && temp<30)
  {
    leds[0].setRGB(255, 251, 0);
    leds[1].setRGB(255, 251, 0);
    leds[2].setRGB(255, 251, 0);
    leds[3].setRGB(255, 251, 0);
    leds[4].setRGB(255, 251, 0);
    leds[5].setRGB(255, 251, 0);
    leds[6].setRGB(255, 251, 0);
    leds[7].setRGB(255, 251, 0);
    leds[8].setRGB(255, 251, 0);
    leds[9].setRGB(255, 251, 0);
    leds[10].setRGB(255, 251, 0);
    leds[11].setRGB(255, 251, 0);
    leds[12].setRGB(255, 251, 0);
    leds[13].setRGB(255, 251, 0);
    leds[14].setRGB(255, 251, 0);
    leds[15].setRGB(255, 251, 0);
    FastLED.show();
  }
  if(temp>=30 && temp<40)
  {
    leds[0].setRGB(255, 60, 0);
    leds[1].setRGB(255, 60, 0);
    leds[2].setRGB(255, 60, 0);
    leds[3].setRGB(255, 60, 0);
    leds[4].setRGB(255, 60, 0);
    leds[5].setRGB(255, 60, 0);
    leds[6].setRGB(255, 60, 0);
    leds[7].setRGB(255, 60, 0);
    leds[8].setRGB(255, 60, 0);
    leds[9].setRGB(255, 60, 0);
    leds[10].setRGB(255, 60, 0);
    leds[11].setRGB(255, 60, 0);
    leds[12].setRGB(255, 60, 0);
    leds[13].setRGB(255, 60, 0);
    leds[14].setRGB(255, 60, 0);
    leds[15].setRGB(255, 60, 0);
    FastLED.show();
  }
  if(temp>=40 && temp<50)
  {
    leds[0].setRGB(255, 0, 0);
    leds[1].setRGB(255, 0, 0);
    leds[2].setRGB(255, 0, 0);
    leds[3].setRGB(255, 0, 0);
    leds[4].setRGB(255, 0, 0);
    leds[5].setRGB(255, 0, 0);
    leds[6].setRGB(255, 0, 0);
    leds[7].setRGB(255, 0, 0);
    leds[8].setRGB(255, 0, 0);
    leds[9].setRGB(255, 0, 0);
    leds[10].setRGB(255, 0, 0);
    leds[11].setRGB(255, 0, 0);
    leds[12].setRGB(255, 0, 0);
    leds[13].setRGB(255, 0, 0);
    leds[14].setRGB(255, 0, 0);
    leds[15].setRGB(255, 0, 0);
    
    FastLED.show();
  }
}